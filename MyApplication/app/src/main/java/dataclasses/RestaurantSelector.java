package dataclasses;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by miste on 12/8/2015.
 */
public class RestaurantSelector
{
    public List<Restaurant> restaurantList = new ArrayList<>();
    public RestaurantSelector()
    {
        //inserting data
        CuisineType ct = new CuisineType();
        ct.italian = true;
        Restaurant r = new Restaurant(9.00, "McCarty Pizza", DiningMode.Diner, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.chinese = true; r = new Restaurant(16.00, "Panda's Garden", DiningMode.Restaurant, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.breakfast = true; r = new Restaurant(18.50, "ISKIP", DiningMode.Restaurant, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.mexican = true; r = new Restaurant(7.50, "Gooey's", DiningMode.FastFood, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.italian = true; r = new Restaurant(30.00, "Bongiorno", DiningMode.Restaurant, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.seafood = true; r = new Restaurant(10.00, "Gull's Nest", DiningMode.Diner, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.vietnamese = true; r = new Restaurant(13.00, "Where's the Pho?", DiningMode.Diner, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.chinese = true; r = new Restaurant(8.80, "Chop Chop", DiningMode.FastFood, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.mexican = true; r = new Restaurant(24.50, "Aguilita", DiningMode.Restaurant, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.breakfast = true; r = new Restaurant(40.00, "Wake Up", DiningMode.Diner, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.seafood = true; r = new Restaurant(11.20, "The Fishery", DiningMode.FastFood, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.vietnamese = true; r = new Restaurant(27.00, "Slurp!", DiningMode.Restaurant, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.italian = true; r = new Restaurant(9.90, "Mario and Luigi", DiningMode.FastFood, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.chinese = true; r = new Restaurant(14.20, "King Panda", DiningMode.Diner, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.breakfast = true; r = new Restaurant(6.00, "Danny's", DiningMode.Restaurant, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.mexican = true; r = new Restaurant(6.50, "Casa de Uva", DiningMode.FastFood, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.seafood = true; r = new Restaurant(50.00, "Captain's Boat", DiningMode.Restaurant, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.vietnamese = true; r = new Restaurant(17.00, "Munch", DiningMode.Restaurant, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.italian = true; r = new Restaurant(7.00, "Bloopers", DiningMode.Diner, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.chinese = true; r = new Restaurant(12.00, "Forever 55", DiningMode.FastFood, ct);
        restaurantList.add(r);
        ct = new CuisineType(); ct.breakfast = true; r = new Restaurant(8.50, "Diner 99", DiningMode.Diner, ct);
        restaurantList.add(r);

        java.util.Collections.sort(restaurantList, new Comparator<Restaurant>() {
            @Override
            public int compare(Restaurant lhs, Restaurant rhs) {
                return lhs.Name.compareTo(rhs.Name);
            }
        });
    }

    public Restaurant ChooseRestaurant(int priceTarget, String diningMode, CuisineType cuisineType)
    {
        Restaurant closestRestaurant = null;
        int cuisineIndex = 0;
        double priceActualTarget = GetPrice(priceTarget);
        for (Restaurant r : restaurantList)
        {
            if (closestRestaurant == null)
                closestRestaurant = r;
            else if(r.Mode.equals(diningMode))
            {
                int compareIndex = cuisineType.CompareRating(r.Type);
                if(compareIndex > cuisineIndex)
                {
                    cuisineIndex = compareIndex;
                    closestRestaurant = r;
                }
                else if (compareIndex == cuisineIndex)
                {
                    if(Math.abs(priceActualTarget - r.Price) < Math.abs(priceActualTarget - closestRestaurant.Price))
                    {
                        closestRestaurant = r;
                    }
                }
            }
        }
        return closestRestaurant;
    }

    public List<Map<String, String>> getRestaurantList()
    {
        List<Map<String, String>> tempList = new ArrayList<Map<String, String>>();

        for (Restaurant r : restaurantList)
        {
            Map<String, String> entry = new HashMap<String, String>(2);
            entry.put("name", r.Name);
            //type is a combination of cuisine type + dining mode
            entry.put("type", r.Type.GetCuisineName() + " " + r.Mode);
            tempList.add(entry);
        }
        return tempList;
    }

    public Restaurant ChooseRandomRestaurant()
    {
        Random rand = new Random();

        int randomNum = rand.nextInt(((restaurantList.size()-1) - 0) + 1) + 0;

        return restaurantList.get(randomNum);
    }

    public Restaurant getRestaurantById(int id)
    {
        return restaurantList.get(id);
    }

    private double GetPrice(int pricePoint)
    {
        if (pricePoint <= 33)
            return 5;
        else if (pricePoint <= 66)
            return 15;
        else
            return 26;
    }

}
