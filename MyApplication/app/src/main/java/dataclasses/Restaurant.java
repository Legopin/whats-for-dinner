package dataclasses;

/**
 * Created by miste on 12/8/2015.
 */
public class Restaurant
{
    Restaurant(double price, String name, String diningMode, CuisineType cuisineType)
    {
        Price = price;
        Name = name;
        Mode = diningMode;
        Type = cuisineType;
    }

    public double Price;
    public String Name;
    public String Mode;
    public CuisineType Type;
}
