package dataclasses;

/**
 * Created by miste on 12/8/2015.
 */
public class CuisineType
{
    public boolean italian=false;
    public boolean chinese=false;
    public boolean breakfast=false;
    public boolean mexican=false;
    public boolean seafood=false;
    public boolean vietnamese=false;

    public int CompareRating(CuisineType otherCuisineType)
    {
        int correctCount = 0;
        if(italian == otherCuisineType.italian)
            correctCount++;
        if(chinese == otherCuisineType.chinese)
            correctCount++;
        if(breakfast == otherCuisineType.breakfast)
            correctCount++;
        if(mexican == otherCuisineType.mexican)
            correctCount++;
        if(seafood == otherCuisineType.seafood)
            correctCount++;
        if(vietnamese == otherCuisineType.vietnamese)
            correctCount++;
        return correctCount;
    }

    public String GetCuisineName()
    {
        if(italian)
            return "Italian";
        else if(breakfast)
            return "Breakfast";
        else if (chinese)
            return "Chinese";
        else if (mexican)
            return "Mexican";
        else if (seafood)
            return "Seafood";
        else
            return "Vietnamese";
    }

    public void Clear()
    {
        breakfast=false;
        chinese=false;
        italian=false;
        seafood=false;
        mexican=false;
        vietnamese=false;
    }

}
