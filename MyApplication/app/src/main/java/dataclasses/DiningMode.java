package dataclasses;

/**
 * Created by miste on 12/8/2015.
 */
public class DiningMode
{
    public static final String FastFood = "Fast Food";
    public static final String Diner = "Diner";
    public static final String Restaurant = "Restaurant";

}
