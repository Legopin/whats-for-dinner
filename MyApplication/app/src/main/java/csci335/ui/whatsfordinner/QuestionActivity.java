package csci335.ui.whatsfordinner;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.w3c.dom.Text;

import dataclasses.CuisineType;

public class QuestionActivity extends AppCompatActivity {


    SeekBar priceBar;
    Spinner spinDiningMode;
    Switch sBreakfast;
    Switch sItalian;
    Switch sMexican;
    Switch sSeafood;
    Switch sChinese;
    Switch sVietnamese;
    Button bSubmit;
    TextView tvMoney;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        priceBar = (SeekBar) findViewById(R.id.sbPriceBar);
        spinDiningMode = (Spinner) findViewById(R.id.spinDiningMode);
        sBreakfast = (Switch) findViewById(R.id.sBreakfast);
        sItalian = (Switch) findViewById(R.id.sItalian);
        sMexican = (Switch) findViewById(R.id.sMexican);
        sSeafood = (Switch) findViewById(R.id.sSeafood);
        sChinese = (Switch) findViewById(R.id.sChinese);
        sVietnamese = (Switch) findViewById(R.id.sVietnamese);
        bSubmit = (Button) findViewById(R.id.bSubmit);
        tvMoney = (TextView) findViewById(R.id.tvMoney);

        //initializes spinner with value from res/strings.xml string array
        ArrayAdapter spinAdapter = ArrayAdapter.createFromResource(this,R.array.dining_mode_array,android.R.layout.simple_spinner_item);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinDiningMode.setAdapter(spinAdapter);

        priceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(progress>66)
                    tvMoney.setText("$$$");
                else if(progress>33)
                    tvMoney.setText("$$");
                else
                    tvMoney.setText("$");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailActivity.class);
                intent.putExtra("random", false);
                intent.putExtra("price", priceBar.getProgress());
                intent.putExtra("dining", spinDiningMode.getSelectedItem().toString());
                intent.putExtra("breakfast", sBreakfast.isChecked());
                intent.putExtra("italian", sItalian.isChecked());
                intent.putExtra("mexican", sMexican.isChecked());
                intent.putExtra("seafood", sSeafood.isChecked());
                intent.putExtra("chinese", sChinese.isChecked());
                intent.putExtra("vietnamese", sVietnamese.isChecked());
                startActivity(intent);
            }
        });
    }

}
