### What is this repository for? ###

This Android 4.4 application was designed as a project for CSCI335 "User Interface and Design". It's purpose is to showcase the basic Android principles we learned in this class in combination with UI design principles.

### Contribution ###

This project was completed by:

Steven Cariaga

Brayden Stewart

Sean Yuan